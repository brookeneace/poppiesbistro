<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		 <div class="parallax" id="parallax-1" data-image-src="<?php the_field('header_image') ?>">
			 <div class="overlay"></div>
		    <div class="caption">
			    <div class="hero-caption">
					   <h1> <span><?php the_field('hero_intro') ?></span><br/>
				    	<?php the_field('hero_large') ?></h1>
				    	<a href="<?php the_field('menu') ?>" target="_blank"><button>View Menu</button></a>
			    </div>
		    </div>
		  </div>
		  <div class="home-content">
			  <div class="home-left">
				<?php
				// Show the selected front page content.
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/page/content', 'front-page' );
					endwhile;
				else :
					get_template_part( 'template-parts/post/content', 'none' );
				endif;
				?>
			  </div>
			  <div class="home-right">
				  <? if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					} ?>
			  </div>
		  </div>
		
		  
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
