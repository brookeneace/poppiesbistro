<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->
		
		<div class="parallax" id="parallax-1" data-image-src="<?php the_field('footer_background_image') ?><?php bloginfo('template_url')?>/assets/images/PoppiesBistro_Exterior.jpg">
		    <div class="caption footer-caption">
		     	<h3>LOCATION</h3><br/><br/>
			 		<p> <?php the_field('location', 'option'); ?></p>
			 	<h3>HOURS</h3><br/><br/>
			 		<p><?php the_field('hours', 'option'); ?></p>
			 		
			 	<h3>Contact</h3><br/><br/>
			 	<?php the_field('contact', 'option'); ?>
			 		

		    </div>
		  </div>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="wrap">
				<div class="footer-disclaimers">&copy; <?php echo date("Y"); ?> Poppies Bistro </div>
				
				<div class="footer-logo">
					<img src="<?php bloginfo('template_url')?>/assets/images/poppies_logo_white-text.png" alt="Poppies Bistro Logo" />
				</div>
				<div class="social">
					<a href="<?php the_field('instagram_link', 'option'); ?>" target="_blank"><i class="fab fa-instagram"></i></a>
					<a href="<?php the_field('facebook_link', 'option'); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
				</div>
			</div><!-- .wrap -->
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
