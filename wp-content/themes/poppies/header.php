<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab|Montserrat:100,300,400,400i,700,700i" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	





	<div class="site-content-contain">
		<div id="content" class="site-content">
			
			<? if ( is_front_page() ) :?>
				<header>
					<img src="<?php bloginfo('template_url')?>/assets/images/poppies_logo_white-text.png" alt="Poppies Bistro Logo" />
					
<!--
					<?php wp_nav_menu( array(
				    'menu'           => 'Main'
					) );?>
-->
				</header>
			<? else :?>
				<header class="interior">
					<div class="overlay"></div>
					<div class="menu-wrap">
					<a href="<?php bloginfo('url')?>"><img src="<?php bloginfo('template_url')?>/assets/images/poppies_logo_white-text.png" alt="Poppies Bistro Logo" /></a>
					
<!--
					<?php wp_nav_menu( array(
				    'menu'           => 'Main'
					) );?>
--></div>
				</header>
				
			<? endif; ?>
				